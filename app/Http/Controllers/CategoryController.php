<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Muestra una lista de las categorías registrados
     * @return json
     */
    public function index(){

        $categories = Category::all();

        // No mostramos toda la información de las categorías, solo lo que interesa mostrar en la tabla
        // y la KEY para identificarlo
        $formatted = $categories->transform(function($category){
            return [
                'key' => $category->key,
                'name' => $category->name
            ];
        });

        return response()->json([
            'status' => 'Success',
            'data' => $formatted
        ], 200);
    }

    public function update(Category $product){

    }

    public function Create(){

    }

    public function delete(Category $product){

    }

}
