<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Product;
use App\Models\Category;
use App\Http\Requests\CreateOrUpdateProductRequest;

class ProductController extends Controller
{

    /**
     * Muestra una lista de los productos registrados
     * @return json
     */
    public function index(){

        $products = Product::all();

        // No mostramos toda la información de los productos, solo lo que interesa mostrar en la tabla
        // y la KEY para identificarlo
        $formatted = $products->transform(function($product){
            return [
                'key' => $product->key,
                'name' => $product->name,
                'description' => $product->description,
                'ref' => $product->ref,
                'price' => $product->price,
                'amount' => $product->amount,
                'category_name' => $product->Category->name,
                'category_key' => $product->Category->key
            ];
        });

        return response()->json([
            'status' => 'Success',
            'data' => $formatted
        ], 200);
    }

    /**
     * Actualizar los datos de un producto en concreto
     * @param  Product              $product
     * @param  UpdateProductRequest $request
     * @return json
     */
    public function update(Product $product, CreateOrUpdateProductRequest $request){

        // Estos campos los asignamos tal cual llegan, ya que han pasado la request personalizada de Laravel
        $product->name = $request->input('name');
        $product->ref = $request->input('ref');
        $product->price = $request->input('price');
        $product->amount = $request->input('amount');
        $product->description = $request->input('description');

        // Recibimos la key de la categoría, y la buscamos. En la request ya se comprueba su existencia
        $category = Category::where('key', $request->input('category'))->first();
        $product->category_id = $category->id;
        $product->save();

        return response()->json([
            'status' => 'Success',
        ], 200);
    }

    /**
     * Creamos un nuevo producto con los datos introducidos (Literalmente
     * la misma funcion de arriba pero con un nuevo objeto, por si en un futuro
     * requieren acciones diferentes)
     * @param  CreateOrUpdateProductRequest $request
     * @return json
     */
    public function create(CreateOrUpdateProductRequest $request){

        // Estos campos los asignamos tal cual llegan, ya que han pasado la request personalizada de Laravel
        $product = new Product();
        $product->name = $request->input('name');
        $product->ref = $request->input('ref');
        $product->key = Str::orderedUuid();
        $product->price = $request->input('price');
        $product->amount = $request->input('amount');
        $product->description = $request->input('description');

        // Recibimos la key de la categoría, y la buscamos. En la request ya se comprueba su existencia
        $category = Category::where('key', $request->input('category'))->first();
        $product->category_id = $category->id;
        $product->save();

        return response()->json([
            'status' => 'Success',
        ], 200);

    }

    /**
     * Borrado de productos en masa (soft delete)
     * @param  Request $request
     * @return json
     */
    public function delete(Request $request){
        $keys = $request->input('keys');

        // Por cada key introducida buscamos el producto, si lo encontramos lo borramos
        foreach($keys as $productKey){
            $product = Product::where('key', $productKey)->first();

            if($product){
                $product->delete();
            }
        }
        return response()->json([
            'status' => 'Success',
        ], 200);
    }

}
