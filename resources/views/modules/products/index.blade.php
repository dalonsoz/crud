@extends('layouts.app')

@section('styles') <!-- Css específico de este módulo -->

@stop

@section('content') <!-- El html en sí -->
    @include('modules.products.content')
@stop

@section('scripts') <!-- El JS del módulo, utilizamos archivos blade como JS para permitir usar directivas de Blade -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="/modules/products/client"></script>
@stop
