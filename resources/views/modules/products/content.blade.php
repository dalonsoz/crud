<div class="container">
	<div class="card">
		<div class="card-header">
			<h4 class="mb-0">Lista de Productos</h4>
		</div>
		<div class="card-body">
			<div id="table-products-toolbar">
				<button data-toggle="modal" data-target="#new-product-modal" class="btn btn-success btn-md">
					<i class="mdi mdi-plus"></i>
					Añadir producto
				</button>
				<button id="remove-products" class="btn btn-danger btn-md">
					<i class="mdi mdi-delete"></i>
					Eliminar seleccionados
				</button>
			</div>
			<table id="table-products" data-classes="table table-hover table-borderless">
				<thead>
					<tr>
                        <th data-field="checkbox" data-checkbox="true"></th>
						<th data-field="name">Nombre</th>
						<th data-field="ref">Referencia</th>
						<th data-field="price">Precio (€)</th>
						<th data-field="amount">Stock (unidades)</th>
						<th data-field="category_name">Categoría</th>
					</tr>
				</thead>
				<tbody style="cursor:pointer">
				</tbody>
			</table>
		</div>
	</div>
</div>

<div id="new-product-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header p-2">
                <h5 class="modal-title">
                    <i class="icon mdi mdi-plus"></i>
                    Añadir producto
                </h5>
            </div>
            <div class="modal-body">
				<div class="row">
					<div class="col-12">
						<h6 class="mb-1" style="color:#0099ab">
							Información producto
						</h6>
						<hr class="mt-0" style="border-color:#0099ab">
						<div class="row">
							<div class="col-12 col-lg-6 form-group">
								<label for="product-new-name">Nombre</label>
								<input required type="text" id="product-new-name" class="form-control"/>
							</div>
							<div class="col-12 col-lg-6 form-group">
								<label for="product-new-category">Categoría</label>
								<select id="product-new-category" class="form-control">
								</select>
							</div>
							<div class="col-12 form-group">
								<label for="product-new-description">Descripción</label>
								<textarea id="product-new-description" rows="3" class="form-control"></textarea>
							</div>
						</div>
					</div>
					<div class="col-12">
						<h6 class="mb-1" style="color:#0099ab">
							Almacén
						</h6>
						<hr class="mt-0" style="border-color:#0099ab">
						<div class="row">
							<div class="col-12 col-lg-6 form-group">
								<label for="product-new-ref">Referencia</label>
								<input type="text" id="product-new-ref" class="form-control"/>
							</div>
							<div class="col-12 col-lg-3 form-group">
								<label for="product-new-price">Precio</label>
								<input type="number" id="product-new-price" class="form-control"/>
							</div>
							<div class="col-12 col-lg-3 form-group">
								<label for="product-new-amount">Stock</label>
								<input type="number" id="product-new-amount" class="form-control"/>
							</div>
						</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
                <div class="d-flex justify-content-between w-100">
                    <div>
						<button type="button" class="btn btn-secondary btn-md" data-dismiss="modal">Cancelar</button>
					</div>
					<div>
						<button type="button" id="products-new-save" class="btn btn-success btn-md">Crear</button>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="view-product-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header p-2">
                <h5 class="modal-title">
                    <i class="icon mdi mdi-information-outline"></i>
                    Detalle producto
                </h5>
            </div>
            <div class="modal-body">
				<div class="row">
					<div class="col-12">
						<h6 class="mb-1" style="color:#0099ab">
							Información producto
						</h6>
						<hr class="mt-0" style="border-color:#0099ab">
						<div class="row">
							<div class="col-12 col-lg-6 form-group">
								<label for="product-view-name">Nombre</label>
								<input type="text" id="product-view-name" class="form-control"/>
							</div>
							<div class="col-12 col-lg-6 form-group">
								<label for="product-view-category">Categoría</label>
								<select id="product-view-category" class="form-control">
								</select>
							</div>
							<div class="col-12 form-group">
								<label for="product-view-description">Descripción</label>
								<textarea id="product-view-description" rows="3" class="form-control"></textarea>
							</div>
						</div>
					</div>
					<div class="col-12">
						<h6 class="mb-1" style="color:#0099ab">
							Almacén
						</h6>
						<hr class="mt-0" style="border-color:#0099ab">
						<div class="row">
							<div class="col-12 col-lg-6 form-group">
								<label for="product-view-ref">Referencia</label>
								<input type="text" id="product-view-ref" class="form-control"/>
							</div>
							<div class="col-12 col-lg-3 form-group">
								<label for="product-view-price">Precio</label>
								<input type="number" id="product-view-price" class="form-control"/>
							</div>
							<div class="col-12 col-lg-3 form-group">
								<label for="product-view-amount">Stock</label>
								<input type="number" id="product-view-amount" class="form-control"/>
							</div>
						</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
                <div class="d-flex justify-content-between w-100">
                    <div>
						<button type="button" class="btn btn-secondary btn-md" data-dismiss="modal">Cancelar</button>
					</div>
					<div>
						<button type="button" id="products-view-save" class="btn btn-success btn-md">Guardar cambios</button>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
