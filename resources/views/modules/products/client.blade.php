const fn_main = {

	tableProducts: $('#table-products'),
	currentProduct: null,

	init: function(){

		// Toastr Plugin para comunicarnos con el usuario tras las peticiones AJAX
		toastr.options = {
			"positionClass": "toast-bottom-center",
			"progressBar": true,
		};

		// Bootstrap table plugin para tablas más bonitas/útiles
		fn_main.tableProducts.bootstrapTable({
			search: true,
			locale: 'es-ES',
            showRefresh: true,
			toolbar: '#table-products-toolbar',
            pagination: true,
            pageNumber: 1,
			icons: {
				refresh: 'mdi mdi-refresh'
			},
            pageSize: 10,
			onRefresh: function(params){
				fn_main.getProductList();
			},
			onClickRow: function(row, $element, field){
				fn_main.loadProductModal(row);
			}
		});

		this.getCategoryList();
		this.getProductList();
		this.events();
	},

	// Event handlers
	events: function(){

		// Al pulsar sobre guardar, recopilamos los datos que nos interesan y los enviamos
		// ya sea para guardar o crear uno nuevo
		$('#products-view-save').on('click', function(){
			fn_main.updateProduct();
		})
		$('#products-new-save').on('click', function(){
			fn_main.createProduct();
		})

		// Al borrar productos, comprobamos si hay seleccionados
		$('#remove-products').on('click', function(){
			let selected = fn_main.tableProducts.bootstrapTable('getSelections');
			if(selected.length === 0){
				toastr.warning('No hay elementos seleccionados', 'Aviso');
			}
			else{

				// Preguntamos primero al usuario por si acaso
				Swal.fire({
					title: '¿Estás seguro?',
					text: "Esto no se puede deshacer",
					icon: 'warning',
					heightAuto: false,
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Si, eliminar',
					cancelButtonText: 'Cancelar'
				}).then((result) => {
					if (result.value) {
						fn_main.deleteProducts(selected);
					}
				})
			}
		})

	},

	// Obtenemos la lista de categorías disponibles y formamos el selector de los modals
	getCategoryList: function(){
		axios.get('/api/categories')
			.then(function(response){

				let html = '<option selected default hidden></option>';
				$.each(response.data.data, function(i, category){
					html += '<option value="'+category.key+'">'+category.name+'</option>'
				})

				// Rellenamos los dos selects con las opciones obtenidas
				$('#product-new-category').html(html);
				$('#product-view-category').html(html);

			})
			.catch(function(error){
				toastr.error('Se ha producido un error al obtener la lista de categorías', 'Error')
			})
	},

	// Obtenemos lista de productos y llamamos a la función que rellena la tabla
	getProductList: function(){
		axios.get('/api/products')
			.then(function(response){

				fn_main.loadTable(response.data.data)

			})
			.catch(function(error){
				toastr.error('Se ha producido un error al obtener la lista de productos', 'Error')
			})
	},

	// Vacía la tabla anterior y la rellena con nuevos datos
	loadTable: function(data){
		fn_main.tableProducts.bootstrapTable('removeAll');
		fn_main.tableProducts.bootstrapTable('load', data);
	},

	// Abre el modal con el detalle del producto seleccionado
	loadProductModal: function(data){
		$('#product-view-name').val(data.name);
		$('#product-view-ref').val(data.ref);
		$('#product-view-price').val(data.price);
		$('#product-view-amount').val(data.amount);
		$('#product-view-description').val(data.description);
		$('#product-view-category').val(data.category_key);
		$('#view-product-modal').modal('show');

		// Al abrir on producto, guardamos su key para guardar cambios más adelante
		fn_main.currentProduct = data.key;
	},

	// PUT para actualizar los datos de un producto
	updateProduct: function(){

		let data = {
			_method: 'PUT', // Axios no deja hacer PUT, lo enviamos como POST pero aquí le indicamos al servidor que es un PUT
			name: $('#product-view-name').val(),
			ref: $('#product-view-ref').val(),
			price: $('#product-view-price').val(),
			amount: $('#product-view-amount').val(),
			description: $('#product-view-description').val(),
			category: $('#product-view-category').val()
		}

		// Por falta de tiempo, no he incluído un plugin de validacion de cliente
		// así que haremos una pequeña comprobación aquí, ya que el servidor es quien
		// se encargará de validar

		let valid = true;
		$.each(data, function(i, field){
			if(field.length === 0){
				toastr.warning('Por favor, rellena todos los campos', 'Aviso')
				valid = false;
				return false;
			}
		})

		if(valid){
			axios.post('/api/products/'+fn_main.currentProduct, data)
				.then(function(response){
					toastr.success('Cambios guardados correctamente', 'Éxito');
					$('#view-product-modal').modal('hide');
					fn_main.getProductList();
				})
				.catch(function(error){
					toastr.error('Se ha producido un error al guardar los cambios', 'Error')
				})
		}
	},

	// Creamos un nuevo producto
	createProduct: function(){

		let data = {
			name: $('#product-new-name').val(),
			ref: $('#product-new-ref').val(),
			price: $('#product-new-price').val(),
			amount: $('#product-new-amount').val(),
			description: $('#product-new-description').val(),
			category: $('#product-new-category').val()
		}

		// Por falta de tiempo, no he incluído un plugin de validacion de cliente
		// así que haremos una pequeña comprobación aquí, ya que el servidor es quien
		// se encargará de validar

		let valid = true;
		$.each(data, function(i, field){
			if(field.length === 0){
				toastr.warning('Por favor, rellena todos los campos', 'Aviso')
				valid = false;
				return false;
			}
		})

		if(valid){
			axios.post('/api/products/', data)
				.then(function(response){
					toastr.success('Producto creado correctamente', 'Éxito');
					$('#new-product-modal').modal('hide');
					fn_main.getProductList();
					fn_main.resetModal();
				})
				.catch(function(error){
					toastr.error('Se ha producido un error al crear el producto', 'Error')
				})
		}
	},

	// Reiniciamos el formulario del modal
	resetModal: function(){
		$('#product-new-name').val('');
		$('#product-new-ref').val('');
		$('#product-new-price').val('');
		$('#product-new-amount').val('');
		$('#product-new-description').val('');
		$('#product-new-category').val(null);
	},

	// Borrado de productos (1 o más)
	deleteProducts: function(selected){
		let keys = []; // Solo nos interesa la key
        $.each(selected, function(i, row){
			keys.push(row.key);
		})

		// Indicamos a axios que es un post, pero es un DELETE
		let data = {
			_method: 'DELETE',
			keys: keys
		}

		axios.post('/api/products', data)
			.then(function(response){
				toastr.success('Productos eliminados correctamente', 'Éxito');
				fn_main.getProductList();
			})
			.catch(function(error){
				toastr.error('Se ha producido un error al eliminar los productos', 'Error')
			})
	}
};


$(document).ready(function () {

	fn_main.init();

});
