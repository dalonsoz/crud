@extends('layouts.app')

@section('styles') <!-- Css específico de este módulo -->

@stop

@section('content') <!-- El html en sí -->
    @include('modules.categories.content')
@stop

@section('scripts') <!-- El JS del módulo, utilizamos archivos blade como JS para permitir usar directivas de Blade -->
    <script src="/modules/categories/client"></script>
@stop
