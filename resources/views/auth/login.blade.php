<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>EurekaCrud</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
    html {
        height: 100%;
    }
    body {
        min-height: 100%;
        font-family: Roboto,sans-serif;
    }
    </style>
</head>
<body class="h-100" style="height:100% !important; background-color: #546E7A">
    <div class="d-flex h-100 flex-row">
        <div class="col-12 col-sm-4 h-100 d-flex flex-column justify-content-center align-items-center" style="background-color: #263238; color:white">
            <h1 class="mt-auto">EurekaCrud</h1>
            <h5>DEMO Crud APP</h5>
            <span class="mt-auto mb-2">
                <a style="color:white; text-decoration:none" href="https://bitbucket.org/dalonsoz/crud/src/master/" target="_blank">With ♥ by David Alonso</a>
            </span>
        </div>
        <div class="col-12 col-sm-8 h-100 d-flex flex-column justify-content-center align-items-center" style=" color:white">
            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="d-flex flex-column align-items-center">
                    <label for="email">Usuario</label>

                    <div>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="d-flex flex-column align-items-center mt-3">
                    <label for="password">Contraseña</label>

                    <div>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group mt-3">
                    <div class="">
                        <button type="submit" class="btn btn-dark btn-block">
                            Iniciar sesión
                        </button>

                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
</html>
