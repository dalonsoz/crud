@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div style="text-align:center" class="card-header">Bienvenido/a  {{ Auth::user()->name }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h5 style="text-align:center" class="mb-3">Accesos rápidos</h5>
                    <div class="d-flex justify-content-around">
                        <a href="/modules/products" target="_self">Gestión de productos</a>
                        <a href="/modules/categories" target="_self">Gestión de categorías</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
