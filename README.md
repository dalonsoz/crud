# EurekaCrud

Una aplicación CRUD de demostración

### Requisitos

- PHP >= 7.2
- MySQL
- Composer -> https://getcomposer.org/
- Extensiones php utilizadas por laravel -> https://laravel.com/docs/6.x#server-requirements

### Instalación

- Clonar el repositorio
- cd /ruta/al/directorio
- composer install
- Copiar el archivo .env.example a .env y configurar la base de datos
- php artisan key:generate (puesto que hemos copiado el proyecto de BitBucket, necesitaremos una key de app)
- php artisan config:cache
- php artisan migrate:fresh --seed (para poblar la BD con datos ficticios)

### Demo

Podemos ejecutar el siguiente comando para iniciar el servidor de desarrollo de laravel en http://127.0.0.1:8000/

```
php artisan serve
```

Todo el proyecto requiere estar autenticado, los credenciales por defecto son:

- User: admin@eurekacrud.com
- Password: demo@2020
