<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Carbon\carbon;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();

        DB::table('products')->insert([
            'name' => 'Magic snow',
            'key' => Str::orderedUuid(),
            'ref' => $faker->ean8(),
            'description' => $faker->paragraph(2),
            'category_id' => 1,
            'price' => 10.99,
            'amount' => $faker->numberBetween(50, 260),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('products')->insert([
            'name' => 'Columpio de metal individual',
            'key' => Str::orderedUuid(),
            'ref' => $faker->ean8(),
            'description' => $faker->paragraph(2),
            'category_id' => 2,
            'price' => 99.95,
            'amount' => $faker->numberBetween(50, 260),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('products')->insert([
            'name' => 'Walkie talkies',
            'key' => Str::orderedUuid(),
            'ref' => $faker->ean8(),
            'description' => $faker->paragraph(2),
            'category_id' => 3,
            'price' => 39.95,
            'amount' => $faker->numberBetween(50, 260),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('products')->insert([
            'name' => 'Bingo',
            'key' => Str::orderedUuid(),
            'ref' => $faker->ean8(),
            'description' => $faker->paragraph(2),
            'category_id' => 4,
            'price' => 49.95,
            'amount' => $faker->numberBetween(50, 260),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
