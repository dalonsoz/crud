<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->middleware('auth');

Auth::routes();

// Navegación web dinámica, por muchos módulos/páginas que se creen no habrá que añadir nuevas rutas
// siempre y cuando sigan el estandard /modules/{name} y tengan los archivos blade necesarios
Route::get('/modules/{name}', function ($name) {
    $view = 'modules.'.$name.'.index';

    // Si no  existe la vista a la que se intenta acceder, redirigimos al menú principal
    if(!view()->exists($view)){
        return redirect('/');
    }
    return view($view);
});
Route::get('/modules/{name}/client', function ($name) {
    $contents = view('modules.'.$name.'.client');
    $response = Response::make($contents, 200);
    $response->header('Content-Type', 'text/javascript');
    return $response;
});

// Ruta de fallback, si no se encuentra la ruta en vez de dar un 404 redirigimos
// al home o al login si no está logeado
Route::get('/{any}', function(){
    return redirect('/');
});
