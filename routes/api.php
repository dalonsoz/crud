<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Rutas de la API, middleware Auth para evitar que usuarios no logeados hagan peticiones
Route::middleware('auth')->group(function(){

	// Gestión de productos
	Route::get('/products', 'ProductController@index');
	Route::put('/products/{product}', 'ProductController@update');
	Route::post('/products', 'ProductController@create');
	Route::delete('/products', 'ProductController@delete');

	// Gestión de categorías
	Route::get('/categories', 'CategoryController@index');
	Route::put('/categories/{category}', 'CategoryController@update');
	Route::post('/categories', 'CategoryController@create');
	Route::delete('/categories', 'CategoryController@delete');
});
